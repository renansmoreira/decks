package deck

import (
	"testing"
)

var (
	shuffleCalls      int
	lastShuffleParams []Card
	shuffleMock       = func(cards []Card) {
		shuffleCalls++
		lastShuffleParams = cards
	}
)

func init() {
	shuffle = shuffleMock
}

func TestCreateNewDeck(t *testing.T) {
	newDeck := New(false)

	wantAmount := 52
	if len(newDeck.Cards) != wantAmount {
		t.Errorf("Expected a new deck with %v cards, but got a new deck with %v cards", wantAmount, len(newDeck.Cards))
	}

	if newDeck.DeckID == "" {
		t.Error("Expected a new deck with an ID, but got empty")
	}
}

func TestCreateNewPartialDeck(t *testing.T) {
	var cases = []struct {
		name   string
		cards  []string
		values []Card
	}{
		{"Multiple cards", []string{"AS", "KD", "2C", "KH"}, []Card{
			{Value: "ACE", Suit: "SPADES", Code: "AS"},
			{Value: "KING", Suit: "DIAMONDS", Code: "KD"},
			{Value: "2", Suit: "CLUBS", Code: "2C"},
			{Value: "KING", Suit: "HEARTS", Code: "KH"},
		}},
		{"Single card", []string{"10C"}, []Card{{Value: "10", Suit: "CLUBS", Code: "10C"}}},
		{"Not uppercase cards", []string{"9h", "AS", "qc"}, []Card{
			{Value: "9", Suit: "HEARTS", Code: "9H"},
			{Value: "ACE", Suit: "SPADES", Code: "AS"},
			{Value: "QUEEN", Suit: "CLUBS", Code: "QC"},
		}},
		{"No cards", []string{}, []Card{}},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			err, newDeck := NewPartial(testCase.cards, false)

			if err != nil {
				t.Errorf("Expected error to be null, but got %v", err)
			}

			wantAmount := len(testCase.cards)
			if len(newDeck.Cards) != wantAmount {
				t.Errorf("Expected a new deck with %v cards, but got a new deck with %v cards", wantAmount, len(newDeck.Cards))
			}

			for index, value := range testCase.values {
				current := newDeck.Cards[index]

				if current != value {
					t.Errorf("Expected the card at position %v to be %v, but got %v", index, value, current)
				}
			}
		})
	}
}

func TestCreateNewPartialDeckWithInvalidCodes(t *testing.T) {
	var cases = []struct {
		name   string
		codes  []string
		wanted string
	}{
		{"All invlid codes", []string{"AF", "10J", "21S"}, "Invalid card codes: AF, 10J, 21S"},
		{"Just one invalid", []string{"AS", "21D", "QH", "KS"}, "Invalid card codes: 21D"},
		{"A bunch of invalid code", []string{"AS", "11D", "QH", "1S", "KV"}, "Invalid card codes: 11D, 1S, KV"},
		{"Invalid codes with spaces", []string{"11D", "1S ", "random", " ** "}, "Invalid card codes: 11D, 1S, RANDOM, **"},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			err, newDeck := NewPartial(testCase.codes, false)

			if err == nil {
				t.Error("Expected an error, but got nothing")
			}

			if err != nil && err.Error() != testCase.wanted {
				t.Errorf("Expected \"%v\" as error message, but got: \"%v\"", err.Error(), testCase.wanted)
			}

			if len(newDeck.Cards) > 0 {
				t.Errorf("Expected an empty deck, but got %v", len(newDeck.Cards))
			}
		})
	}
}

func TestCreateShuffledDeck(t *testing.T) {
	shuffleCalls = 0
	newDeck := New(true)

	wantAmount := 52
	if len(newDeck.Cards) != wantAmount {
		t.Errorf("Expected a new deck with %v cards, but got a new deck with %v cards", wantAmount, len(newDeck.Cards))
	}

	wanted := true
	if newDeck.Shuffled != wanted {
		t.Error("Expected a new shuffled deck, but it is not")
	}

	wantedShuffleCalls := 1
	if shuffleCalls != wantedShuffleCalls {
		t.Error("Expected the shuffler to be called once")
	}

	wantedShuffledCards := 52
	if len(lastShuffleParams) != wantedShuffledCards {
		t.Errorf("Expected to shuffle %v cards, but got %v", wantedShuffledCards, len(lastShuffleParams))
	}

	for index, card := range lastShuffleParams {
		if newDeck.Cards[index] != card {
			t.Errorf("Expected the last shuffle param at position %v to be %v, but got %v", index, card, newDeck.Cards[index])
		}
	}
}

func TestCreatePartialShuffledDeck(t *testing.T) {
	shuffleCalls = 0

	err, newDeck := NewPartial([]string{"10H", "KS"}, true)

	if err != nil {
		t.Errorf("Expected error to be null, but got %v", err)
	}

	wantAmount := 2
	if len(newDeck.Cards) != wantAmount {
		t.Errorf("Expected a new deck with %v cards, but got a new deck with %v cards", wantAmount, len(newDeck.Cards))
	}

	wanted := true
	if newDeck.Shuffled != wanted {
		t.Error("Expected a new shuffled deck, but it is not")
	}

	wantedShuffleCalls := 1
	if shuffleCalls != wantedShuffleCalls {
		t.Error("Expected the shuffler to be called once")
	}

	wantedShuffledCards := 2
	if len(lastShuffleParams) != wantedShuffledCards {
		t.Errorf("Expected to shuffle %v cards, but got %v", wantedShuffledCards, len(lastShuffleParams))
	}

	for index, card := range lastShuffleParams {
		if newDeck.Cards[index] != card {
			t.Errorf("Expected the last shuffle param at position %v to be %v, but got %v", index, card, newDeck.Cards[index])
		}
	}
}

func TestDrawCards(t *testing.T) {
	var cases = []struct {
		name   string
		amount int
	}{
		{"Multiple cards", 5},
		{"All cards", 52},
		{"Keeping just one card", 51},
		{"One card", 1},
		{"No cards", 0},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			newDeck := New(false)
			initialSize := len(newDeck.Cards)
			wantCards := newDeck.Cards[:testCase.amount]

			drawnCards := newDeck.Draw(testCase.amount)

			if len(drawnCards) != testCase.amount {
				t.Errorf("Expected to draw %v cards, but got %v", testCase.amount, len(drawnCards))
			}

			if len(newDeck.Cards) != initialSize-testCase.amount {
				t.Errorf("Expected to have a deck with %v remaining cards, but got %v", initialSize-testCase.amount, len(newDeck.Cards))
			}

			for index, expected := range wantCards {
				if drawnCards[index] != expected {
					t.Errorf("Expected the draw card to be %v, but got %v", expected, drawnCards[index])
				}
			}
		})
	}
}

func TestDrawWithInvalidAmounts(t *testing.T) {
	var cases = []struct {
		name              string
		amount            int
		expectedAmount    int
		expectedRemaining int
	}{
		{"More then all cards", 200, 52, 0},
		{"Negative cards", -12, 0, 52},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			newDeck := New(false)
			wantCards := newDeck.Cards

			drawnCards := newDeck.Draw(testCase.amount)

			if len(drawnCards) != testCase.expectedAmount {
				t.Errorf("Expected to draw %v cards, but got %v", testCase.expectedAmount, len(drawnCards))
			}

			if len(newDeck.Cards) != testCase.expectedRemaining {
				t.Errorf("Expected to have a deck with %v remaining cards, but got %v", testCase.expectedRemaining, len(newDeck.Cards))
			}

			if len(drawnCards) > 0 {
				for index, expected := range wantCards {
					if drawnCards[index] != expected {
						t.Errorf("Expected the draw card to be %v, but got %v", expected, drawnCards[index])
					}
				}
			}
		})
	}
}

func TestDrawMultipleTimesFromTheSameDeck(t *testing.T) {
	newDeck := New(false)
	expectedTotal := 9
	initialSize := len(newDeck.Cards)
	wantCards := newDeck.Cards[5:9]
	newDeck.Draw(3)
	newDeck.Draw(2)

	drawnCards := newDeck.Draw(4)

	wantTotal := 4
	if len(drawnCards) != wantTotal {
		t.Errorf("Expected to draw %v cards, but got %v", wantTotal, len(drawnCards))
	}

	if len(newDeck.Cards) != initialSize-expectedTotal {
		t.Errorf("Expected to have a deck with %v remaining cards, but got %v", initialSize-expectedTotal, len(newDeck.Cards))
	}

	for index, expected := range wantCards {
		if drawnCards[index] != expected {
			t.Errorf("Expected the draw card to be %v, but got %v", expected, drawnCards[index])
		}
	}
}
