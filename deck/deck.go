package deck

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

var shuffle = func(cards []Card) {
	for index := range cards {
		j := rand.Intn(index + 1)
		cards[index], cards[j] = cards[j], cards[index]
	}
}

type Deck struct {
	gorm.Model
	DeckID   string
	Shuffled bool
	Cards    []Card
}

type Card struct {
	gorm.Model
	DeckID string
	Value  string
	Suit   string
	Code   string
}

var suitsMap = map[string]string{
	"C": "CLUBS",
	"D": "DIAMONDS",
	"S": "SPADES",
	"H": "HEARTS",
}

var letterCardsMap = map[string]string{
	"A": "ACE",
	"J": "JACK",
	"Q": "QUEEN",
	"K": "KING",
}

func New(shuffled bool) Deck {
	id := uuid.NewString()
	cards := createDefaultCards()

	if shuffled {
		shuffle(cards)
	}

	return Deck{
		DeckID:   id,
		Shuffled: shuffled,
		Cards:    cards,
	}
}

func NewPartial(cardCodes []string, shuffled bool) (error, Deck) {
	id := uuid.NewString()
	err, cards := createCards(cardCodes)
	if err != nil {
		return err, Deck{
			Cards: []Card{},
		}
	}

	if shuffled {
		shuffle(cards)
	}

	return nil, Deck{
		DeckID:   id,
		Shuffled: shuffled,
		Cards:    cards,
	}
}

func (deck *Deck) Draw(amount int) (drawnCards []Card) {
	if amount < 0 {
		return []Card{}
	}

	if amount >= len(deck.Cards) {
		drawnCards = deck.Cards
		deck.Cards = []Card{}
	} else {
		drawnCards = deck.Cards[:amount]
		deck.Cards = deck.Cards[amount:]
	}

	return
}

func createDefaultCards() (cards []Card) {
	cards = []Card{}

	for _, suit := range suitsMap {
		cards = append(cards, Card{
			Value: "ACE",
			Suit:  suit,
			Code:  "A" + suit[:1],
		})

		for index := 2; index <= 10; index++ {
			value := fmt.Sprint(index)

			cards = append(cards, Card{
				Value: value,
				Suit:  suit,
				Code:  value + suit[:1],
			})
		}

		cards = append(cards, Card{
			Value: "JACK",
			Suit:  suit,
			Code:  "J" + suit[:1],
		})
		cards = append(cards, Card{
			Value: "QUEEN",
			Suit:  suit,
			Code:  "Q" + suit[:1],
		})
		cards = append(cards, Card{
			Value: "KING",
			Suit:  suit,
			Code:  "K" + suit[:1],
		})
	}

	return
}

func createCards(cardCodes []string) (err error, cards []Card) {
	if len(cardCodes) == 0 {
		return nil, []Card{}
	}

	cards = []Card{}
	invalidCodes := []string{}

	for _, code := range cardCodes {
		codeToParse := strings.ToUpper(strings.TrimSpace(code))
		err, card := translateFromCode(codeToParse)
		cards = append(cards, card)

		if err != nil {
			invalidCodes = append(invalidCodes, codeToParse)
		}
	}

	if len(invalidCodes) > 0 {
		return fmt.Errorf("Invalid card codes: %v", strings.Join(invalidCodes, ", ")), []Card{}
	}

	return
}

func translateFromCode(code string) (error, Card) {
	suit := suitsMap[code[len(code)-1:]]

	if suit == "" {
		return fmt.Errorf("Invalid card code"), Card{}
	}

	valueCode := code[:len(code)-1]
	var value string

	if intValue, err := strconv.Atoi(valueCode); err == nil {
		if intValue > 10 || intValue < 2 {
			value = ""
		} else {
			value = valueCode
		}
	} else {
		value = letterCardsMap[valueCode]
	}

	if value == "" {
		return fmt.Errorf("Invalid card code"), Card{}
	}

	return nil, Card{
		Value: value,
		Suit:  suit,
		Code:  code,
	}
}
