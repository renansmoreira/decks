package service

import (
	"log"
	"renansmoreira/deck/config"
	"renansmoreira/deck/deck"
	"strings"
)

func CreateDeck(request *CreateDeckRequest) (err error, response *CreateDeckResponse) {
	var newDeck deck.Deck

	if len(request.Cards) > 0 {
		err, newDeck = deck.NewPartial(strings.Split(request.Cards, ","), request.Shuffled)
		if err != nil {
			log.Println(err)
			return err, nil
		}
	} else {
		newDeck = deck.New(request.Shuffled)
	}

	db := config.GetDb()
	err = db.Create(&newDeck).Error
	if err != nil {
		log.Println(err)
		return err, nil
	}

	response = &CreateDeckResponse{
		ID:        newDeck.DeckID,
		Shuffled:  newDeck.Shuffled,
		Remaining: len(newDeck.Cards),
	}

	return
}
