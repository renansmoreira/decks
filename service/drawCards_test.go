package service_test

import (
	"renansmoreira/deck/config"
	"renansmoreira/deck/service"
	"renansmoreira/deck/test/fixture"
	"testing"
)

func TestDrawCards(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateNewDeck()

	err, response := service.DrawCards(&service.DrawDeckRequest{
		ID:     newDeck.DeckID,
		Amount: 21,
	})

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	wantedAmount := 21
	if len(response) != wantedAmount {
		t.Errorf("Expected to draw %v, but got %v", wantedAmount, len(response))
	}
}

func TestDrawEmptyCards(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateEmptyDeck()

	err, response := service.DrawCards(&service.DrawDeckRequest{
		ID:     newDeck.DeckID,
		Amount: 10,
	})

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	if response == nil {
		t.Error("Expected to draw an empty array, but got nil")
	}

	wantedAmount := 0
	if len(response) != wantedAmount {
		t.Errorf("Expected to draw %v, but got %v", wantedAmount, len(response))
	}
}

func TestDrawInvalidDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	err, response := service.DrawCards(&service.DrawDeckRequest{
		ID:     "invalid-deck-id",
		Amount: 21,
	})

	if err == nil {
		t.Error("Expected error to be not nil, but got nil")
	}

	if err.Error() != "Deck not found" {
		t.Errorf("Expected error to be 'Deck not found', but got %+q", err.Error())
	}

	if response != nil {
		t.Errorf("Expected response to be nil, but got %+q", response)
	}
}
