package service_test

import (
	"renansmoreira/deck/config"
	"renansmoreira/deck/deck"
	"renansmoreira/deck/service"
	"testing"
)

func TestCreateDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	err, response := service.CreateDeck(&service.CreateDeckRequest{Shuffled: false})

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	if response.ID == "" {
		t.Error("Expected a new deck with an ID, but got empty")
	}

	wantRemaining := 52
	if response.Remaining != wantRemaining {
		t.Errorf("Expected a new deck with %v cards, but got %v", wantRemaining, response.Remaining)
	}
}

func TestCreateNewPartialDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	err, response := service.CreateDeck(&service.CreateDeckRequest{
		Shuffled: false,
		Cards:    "AS,KD,AC,2C,KH,10D",
	})

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	if response.ID == "" {
		t.Error("Expected a new deck with an ID, but got empty")
	}

	wantAmount := 6
	if response.Remaining != wantAmount {
		t.Errorf("Expected a new deck with %v cards, but got %v", wantAmount, response.Remaining)
	}
}

func TestShouldSaveTheNewDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	_, response := service.CreateDeck(&service.CreateDeckRequest{Shuffled: false})

	var wanted deck.Deck
	config.GetDb().Preload("Cards").First(&wanted, "deck_id = ?", response.ID)
	if response.ID != wanted.DeckID {
		t.Errorf("Expected a new deck with ID %q, but got %q", response.ID, wanted.DeckID)
	}

	if response.Remaining != len(wanted.Cards) {
		t.Errorf("Expected a new deck with %v cards, but got %v", len(wanted.Cards), response.Remaining)
	}
}

func TestCreateNewPartialDeckWithInvalidCodes(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	err, response := service.CreateDeck(&service.CreateDeckRequest{
		Shuffled: false,
		Cards:    "AS, 11D, 1S, KH, random code   , *&$%& ",
	})

	if response != nil {
		t.Errorf("Expected response to be nil, but got %v", response)
	}

	wantErr := "Invalid card codes: 11D, 1S, RANDOM CODE, *&$%&"
	if err.Error() != wantErr {
		t.Errorf("Expected response to be %q, but got %q", wantErr, err.Error())
	}
}
