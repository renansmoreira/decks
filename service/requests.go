package service

type CreateDeckRequest struct {
	Shuffled bool   `json:"shuffled"`
	Cards    string `json:"cards"`
}

type DrawDeckRequest struct {
	ID     string `json:"id"`
	Amount int    `json:"amount"`
}
