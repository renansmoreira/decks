package service

import (
	"fmt"
	"renansmoreira/deck/config"
	"renansmoreira/deck/deck"
)

func OpenDeck(id string) (err error, response *OpenDeckResponse) {
	db := config.GetDb()
	var openedDeck deck.Deck

	if err = db.Preload("Cards").First(&openedDeck, "deck_id = ?", id).Error; err != nil {
		return fmt.Errorf("Deck not found"), nil
	}

	var cards []CardResponse

	for _, card := range openedDeck.Cards {
		cards = append(cards, CardResponse{
			Value: card.Value,
			Suit:  card.Suit,
			Code:  card.Code,
		})
	}

	if cards == nil {
		cards = []CardResponse{}
	}

	response = &OpenDeckResponse{
		ID:        openedDeck.DeckID,
		Shuffled:  openedDeck.Shuffled,
		Remaining: len(openedDeck.Cards),
		Cards:     cards,
	}

	return
}
