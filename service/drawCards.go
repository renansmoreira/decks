package service

import (
	"fmt"
	"log"
	"renansmoreira/deck/config"
	"renansmoreira/deck/deck"
)

func DrawCards(request *DrawDeckRequest) (err error, response []DrawDeckResponse) {
	db := config.GetDb()
	var openedDeck deck.Deck

	if err = db.Preload("Cards").First(&openedDeck, "deck_id = ?", request.ID).Error; err != nil {
		log.Println(err)
		return fmt.Errorf("Deck not found"), nil
	}

	drawnCards := openedDeck.Draw(request.Amount)

	for _, card := range drawnCards {
		if err = db.Model(&deck.Card{}).Delete(&card).Error; err != nil {
			log.Println(err)
			return fmt.Errorf("Failed to draw cards"), nil
		}
	}

	for _, card := range drawnCards {
		response = append(response, DrawDeckResponse{
			Value: card.Value,
			Suit:  card.Suit,
			Code:  card.Code,
		})
	}

	if response == nil {
		response = []DrawDeckResponse{}
	}

	return
}
