package service_test

import (
	"renansmoreira/deck/config"
	"renansmoreira/deck/service"
	"renansmoreira/deck/test/fixture"
	"testing"
)

func TestOpenDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateNewDeck()

	err, response := service.OpenDeck(newDeck.DeckID)

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	if response.ID != newDeck.DeckID {
		t.Errorf("Expected to open the deck with ID %v, but got %v", newDeck.ID, response.ID)
	}

	if response.Shuffled {
		t.Error("Expected to open a unshuffled deck, but is it shuffled")
	}

	wantCardCount := len(newDeck.Cards)
	if len(response.Cards) != wantCardCount {
		t.Errorf("Expected to open a deck with %v cards, but got %v", wantCardCount, len(response.Cards))
	}
}

func TestOpenEmptyDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateEmptyDeck()

	err, response := service.OpenDeck(newDeck.DeckID)

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	if response.ID != newDeck.DeckID {
		t.Errorf("Expected to open the deck with ID %v, but got %v", newDeck.ID, response.ID)
	}

	if response.Shuffled {
		t.Error("Expected to open a unshuffled deck, but is it shuffled")
	}

	wantCardCount := 0
	if len(response.Cards) != wantCardCount {
		t.Errorf("Expected to open a deck with %v cards, but got %v", wantCardCount, len(response.Cards))
	}
}

func TestOpenInvalidDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	err, response := service.OpenDeck("random id")

	if err == nil {
		t.Error("Expected error to be not nil, but got nil")
	}

	wantedError := "Deck not found"
	if err.Error() != wantedError {
		t.Errorf("Expected error to be %q, but got %q", wantedError, err.Error())
	}

	if response != nil {
		t.Errorf("Expected response to be nil, but got %v", response)
	}
}
