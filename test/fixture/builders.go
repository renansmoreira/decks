package fixture

import (
	"log"
	"renansmoreira/deck/config"
	"renansmoreira/deck/deck"

	"github.com/google/uuid"
)

func CreateNewDeck() deck.Deck {
	db := config.GetDb()
	newDeck := deck.New(false)

	err := db.Create(&newDeck).Error
	if err != nil {
		log.Fatalf("Failed to create a new deck %v", err)
	}

	return newDeck
}

func CreateEmptyDeck() deck.Deck {
	db := config.GetDb()
	newDeck := deck.Deck{
		DeckID:   uuid.NewString(),
		Shuffled: false,
	}

	err := db.Create(&newDeck).Error
	if err != nil {
		log.Fatalf("Failed to create a new deck %v", err)
	}

	return newDeck
}
