FROM golang:1.21

WORKDIR /app

COPY config ./config
COPY deck ./deck
COPY handler ./handler
COPY service ./service
COPY test ./test
COPY go.mod go.sum *.go ./

RUN go mod download
RUN go build -o app
RUN go test ./...

ENV PORT=7192
EXPOSE 7192

CMD ["./app"]