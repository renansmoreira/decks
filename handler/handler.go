package handler

import "net/http"

func Initialize() {
	http.HandleFunc("/deck", DeckRequests)
	http.HandleFunc("/draw", DrawRequests)
}
