package handler_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"renansmoreira/deck/config"
	"renansmoreira/deck/handler"
	"renansmoreira/deck/service"
	"renansmoreira/deck/test/fixture"
	"testing"
)

func TestHandlerDrawCards(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateNewDeck()

	requestBody, _ := json.Marshal(service.DrawDeckRequest{
		ID:     newDeck.DeckID,
		Amount: 5,
	})

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/draw", bytes.NewReader(requestBody))
	defer req.Body.Close()

	handler.DrawRequests(w, req)
	defer w.Result().Body.Close()

	decoder := json.NewDecoder(w.Result().Body)
	var response []service.DrawDeckResponse
	err := decoder.Decode(&response)

	wantCode := http.StatusOK
	if w.Result().StatusCode != wantCode {
		t.Errorf("Expected status code to be %03d, but got %03d", wantCode, w.Result().StatusCode)
	}

	if err != nil {
		t.Errorf("Expected error to be nil, but got %+q", err)
	}

	wantedAmount := 5
	if len(response) != wantedAmount {
		t.Errorf("Expected to draw %v, but got %v", wantedAmount, len(response))
	}
}

func TestHandlerDrawInvalidParameters(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	requestBody, _ := json.Marshal(service.DrawDeckRequest{
		ID:     "random id",
		Amount: 7,
	})

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/draw", bytes.NewReader(requestBody))
	defer req.Body.Close()

	handler.DrawRequests(w, req)
	defer w.Result().Body.Close()

	body, _ := io.ReadAll(w.Result().Body)

	wantedCode := http.StatusNotFound
	if w.Result().StatusCode != wantedCode {
		t.Errorf("Expected status code to be %03d, but got %03d", wantedCode, w.Result().StatusCode)
	}

	wantedMessage := "Deck not found\n"
	if string(body) != wantedMessage {
		t.Errorf("Expected response to be %q, but got %q", wantedMessage, string(body))
	}
}
