package handler_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"renansmoreira/deck/config"
	"renansmoreira/deck/handler"
	"renansmoreira/deck/service"
	"renansmoreira/deck/test/fixture"
	"testing"

	"github.com/google/uuid"
)

func TestHandlerCreateDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	requestBody, _ := json.Marshal(service.CreateDeckRequest{Shuffled: true})

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/deck", bytes.NewReader(requestBody))
	defer req.Body.Close()

	handler.DeckRequests(w, req)
	defer w.Result().Body.Close()

	decoder := json.NewDecoder(w.Result().Body)
	var response service.CreateDeckResponse
	decoder.Decode(&response)

	wantCode := http.StatusCreated
	if w.Result().StatusCode != wantCode {
		t.Errorf("Expected status code to be %03d, but got %03d", wantCode, w.Result().StatusCode)
	}

	if response.ID == "" {
		t.Error("Expected a new deck with an ID, but got empty")
	}

	if !response.Shuffled {
		t.Error("Expected a new deck to be shuffled, but it is not")
	}

	wantedAmount := 52
	if response.Remaining != wantedAmount {
		t.Errorf("Expected a new deck with %v cards, but got %v", wantedAmount, response.Remaining)
	}
}

func TestHandlerCreateInvlalidDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	requestBody, _ := json.Marshal(service.CreateDeckRequest{
		Shuffled: true,
		Cards:    "AS, ¨%$, QH, TEST",
	})

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/deck", bytes.NewReader(requestBody))
	defer req.Body.Close()

	handler.DeckRequests(w, req)
	defer w.Result().Body.Close()

	body, _ := io.ReadAll(w.Result().Body)

	wantedCode := http.StatusBadRequest
	if w.Result().StatusCode != wantedCode {
		t.Errorf("Expected status code to be %03d, but got %03d", wantedCode, w.Result().StatusCode)
	}

	wantedMessage := "Invalid card codes: ¨%$, TEST\n"
	if string(body) != wantedMessage {
		t.Errorf("Expected response to be %q, but got %q", wantedMessage, string(body))
	}
}

func TestHandlerOpenDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()
	newDeck := fixture.CreateNewDeck()

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/deck?id="+newDeck.DeckID, http.NoBody)
	defer req.Body.Close()

	handler.DeckRequests(w, req)
	defer w.Result().Body.Close()

	decoder := json.NewDecoder(w.Result().Body)
	var response service.OpenDeckResponse
	decoder.Decode(&response)

	wantCode := http.StatusOK
	if w.Result().StatusCode != wantCode {
		t.Errorf("Expected status code to be %03d, but got %03d", wantCode, w.Result().StatusCode)
	}

	if response.ID != newDeck.DeckID {
		t.Errorf("Expected to open the deck with ID %v, but got %v", newDeck.ID, response.ID)
	}

	if response.Shuffled {
		t.Error("Expected to open a unshuffled deck, but is it shuffled")
	}

	wantCardCount := len(newDeck.Cards)
	if len(response.Cards) != wantCardCount {
		t.Errorf("Expected to open a deck with %v cards, but got %v", wantCardCount, len(response.Cards))
	}
}

func TestHandlerOpenInvalidDeck(t *testing.T) {
	config.InitializeTest()
	defer config.CleanupTest()

	var cases = []struct {
		name          string
		endpoint      string
		wantedCode    int
		wantedMessage string
	}{
		{"Invalid id", "/deck?id=" + uuid.NewString(), http.StatusNotFound, "Deck not found\n"},
		{"No id", "/deck", http.StatusBadRequest, "Invalid parameters\n"},
		{"Empty id", "/deck?id=", http.StatusBadRequest, "Invalid parameters\n"},
		{"Unknown query param", "/deck?random=", http.StatusBadRequest, "Invalid parameters\n"},
		{"Invalid param", "/deck?=", http.StatusBadRequest, "Invalid parameters\n"},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, testCase.endpoint, http.NoBody)
			defer req.Body.Close()

			handler.DeckRequests(w, req)
			defer w.Result().Body.Close()

			body, _ := io.ReadAll(w.Result().Body)

			if w.Result().StatusCode != testCase.wantedCode {
				t.Errorf("Expected status code to be %03d, but got %03d", testCase.wantedCode, w.Result().StatusCode)
			}

			if string(body) != testCase.wantedMessage {
				t.Errorf("Expected response to be %q, but got %q", testCase.wantedMessage, string(body))
			}
		})
	}
}
