package handler

import (
	"encoding/json"
	"net/http"
	"renansmoreira/deck/service"
)

func DeckRequests(w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		decoder := json.NewDecoder(req.Body)
		var request service.CreateDeckRequest
		err := decoder.Decode(&request)
		if err != nil {
			http.Error(w, "Invalid parameters", http.StatusBadRequest)
			return
		}

		err, response := service.CreateDeck(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		json, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Failed to create a new deck", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		w.Write(json)
		return
	}

	if req.Method == "GET" {
		id := req.URL.Query().Get("id")
		if id == "" {
			http.Error(w, "Invalid parameters", http.StatusBadRequest)
			return
		}

		err, response := service.OpenDeck(id)
		if err != nil {
			http.Error(w, "Deck not found", http.StatusNotFound)
			return
		}

		json, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Failed to create a new deck", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}

	http.NotFound(w, req)
}
