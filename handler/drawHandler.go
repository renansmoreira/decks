package handler

import (
	"encoding/json"
	"net/http"
	"renansmoreira/deck/service"
)

func DrawRequests(w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		decoder := json.NewDecoder(req.Body)
		var request service.DrawDeckRequest
		err := decoder.Decode(&request)
		if err != nil {
			http.Error(w, "Invalid parameters", http.StatusBadRequest)
			return
		}

		err, response := service.DrawCards(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		json, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Failed to create a new deck", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	}

	http.NotFound(w, req)
}
