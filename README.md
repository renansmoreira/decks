# Decks API

This project creates a new API to create and draw cards from decks.

Built with Go 1.21.

## Run
To start the API you need to run:
```shell
go run main.go
```

Or you can build and run from the binary:
```shell
go build -o app && ./app
```

Or you can build it to run in a Docker image using this command:
```shell
  docker build -t deck:latest . && docker run -p 7192:7192 deck
```

You don't need to setup a database or migrations, the app will take care of everything using SQLite local DB.

## Build
If you just want to build, you can achieve this using the following command:
```shell
go build -o app
```

## Tests
You can run all automated tests using this command:
```shell
go test ./...
```

Also you can run manual tests using the `requests.http` file with RestClient VSCode plugin or similar like JetBrains HTTP Client.

## Endpoint docs
All docs here are using the default app port, remember to change it if you prefer another one.

### Create a new deck
```
POST http://localhost:7192/deck
```

**Parameters**
| Name | Required | Type | Description |
|-:|:-:|:-:|:-|
| shuffled | no | boolean | Indicates whether the new deck should have shuffled cards or not. Default: `false` |
| cards | no | string | Specify the cards to create for the new deck using the following format `AS, 8H, KC, 10D`. Defaults to the standard 52-card deck if empty or null. Default: `null` |

**Response**
| Attribute | Type | Description |
|-:|:-:|:-|
| deck_id | string | The new deck ID. |
| shuffled | boolean | Indicates if the deck is shuffled or not. |
| remaining | int | The number of remaining cards in the deck. |

### Get existing deck
```
GET http://localhost:7192/deck?id=
```

**Parameters**
| Name | Required | Type | Description |
|-:|:-:|:-:|:-|
| id | yes | string | Deck ID to get. |

**Response**
| Attribute | Type | Description |
|-:|:-:|:-|
| deck_id | string | The new deck ID. |
| shuffled | boolean | Indicates if the deck is shuffled or not. |
| remaining | int | The number of remaining cards in the deck. |
| cards | array | Array of Card object representing all the cards available in the deck. |

**Card object**
| Attribute | Type | Description |
|-:|:-:|:-|
| value | string | Card value. |
| suit | string | Card suit. |
| code | string | String representation of the card containing it's value and suit. |

### Draw cards from the deck
```
POST http://localhost:7192/draw
```

**Parameters**
| Name | Required | Type | Description |
|-:|:-:|:-:|:-|
| id | yes | string | Deck ID to draw cards. |
| amount | no | int | The number of cards to draw. Does nothing for values that are smaller than 1. Default: `0` |

**Response**

Returns an array of the card object described before.