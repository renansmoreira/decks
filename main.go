package main

import (
	"log"
	"net/http"
	"os"
	"renansmoreira/deck/config"
	"renansmoreira/deck/handler"
)

func main() {
	config.Initialize()
	handler.Initialize()

	port := os.Getenv("PORT")

	if port == "" {
		port = "7192"
	}

	log.Println("Starting a new server on port", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal(err)
	}
}
