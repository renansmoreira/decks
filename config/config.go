package config

import (
	"log"
	"os"
	"renansmoreira/deck/deck"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func Initialize() {
	createDb("./database.db")
}

func InitializeTest() {
	createDb("./testdatabase.db")
}

func CleanupTest() {
	os.Remove("./testdatabase.db")
}

func createDb(name string) {
	var err error

	db, err = gorm.Open(sqlite.Open(name), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
		panic("Database connection failed")
	}

	db.AutoMigrate(&deck.Deck{})
	db.AutoMigrate(&deck.Card{})
}

func GetDb() *gorm.DB {
	if db == nil {
		panic("Database not connected")
	}

	return db
}
